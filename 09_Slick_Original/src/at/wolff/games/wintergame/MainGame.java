package at.wolff.games.wintergame;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.Sys;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tests.AnimationTest;

public class MainGame extends BasicGame {

	private List<Actor> actors;

	public MainGame(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		// gezeichnet
		for (Actor s : this.actors) {
			s.render(graphics);
		}

	}

	@Override
	public void init(GameContainer arg0) throws SlickException {
		this.actors = new ArrayList<>();
		// wird 1 mal aufgerufen
		this.actors.add(new CircleActor(350, 100));
		this.actors.add(new OvalActor(100, 500));
		// this.actors.add(new Homer());
		this.actors.add(new RectangleActor(100, 100));

		for (int i = 0; i < 50; i++) {
			this.actors.add(new Snowflake("big"));
			this.actors.add(new Snowflake("small"));
			this.actors.add(new Snowflake("medium"));
		}
		this.actors.add(new ShootingStar(30, 500));
		this.actors.add(new Snowman());

	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		// delta = Zeit seit dem letzten Aufruf
		for (Actor s : this.actors) {
			s.update(gc, delta);
		}

	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MainGame("Wintergame"));
			container.setDisplayMode(800, 600, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
