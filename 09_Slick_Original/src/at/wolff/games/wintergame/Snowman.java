package at.wolff.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

public class Snowman implements Actor {

	private float x, y = 10;
	private float xCircle, yCircle = 50;
	private Image image;
	private boolean keyDownPressed, keyUpPressed, keySpacePressed, keyRightPressed, keyLeftPressed,
			ballIsMoving = false;

	Snowman() throws SlickException {
		this.image = new Image("testdata/snowman.png");

	}

	@Override
	public void update(GameContainer gc, int delta) {
		// TODO Auto-generated method stub

		keySpacePressed = gc.getInput().isKeyDown(Input.KEY_SPACE);
		keyUpPressed = gc.getInput().isKeyDown(Input.KEY_UP);
		keyDownPressed = gc.getInput().isKeyDown(Input.KEY_DOWN);
		keyRightPressed = gc.getInput().isKeyDown(Input.KEY_RIGHT);
		keyLeftPressed = gc.getInput().isKeyDown(Input.KEY_LEFT);

		if (keyDownPressed == true) {
			this.y++;
		} else if (keyUpPressed == true) {
			this.y--;
		} else if (keyRightPressed == true) {
			this.x++;
		} else if (keyLeftPressed == true) {
			this.x--;
		} else if (keySpacePressed == true) {
			this.ballIsMoving = true;
			this.xCircle = this.x;
			this.yCircle = this.y;
		}

		if (ballIsMoving == true && xCircle < 810) {
			this.xCircle++;
		}

	}

	@Override
	public void render(Graphics graphics) {
		// TODO Auto-generated method stub
		graphics.drawImage(image, x, y);
		if (this.ballIsMoving == true) {
			graphics.fillOval(xCircle + 50 , yCircle + 50, 20, 20);
		}

	}

}
