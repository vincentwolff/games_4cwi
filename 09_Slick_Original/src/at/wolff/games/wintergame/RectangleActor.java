package at.wolff.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class RectangleActor implements Actor {
	
	private double x, y;
	private boolean rectUp = false;
	private boolean rectDown = true;
	private boolean rectLeft = false;
	private boolean rectRight = true;

	public RectangleActor(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}

	public void update(GameContainer gc, int delta) {

		if (this.rectRight == true) {
			this.x++;
			if (this.x == 700) {
				this.rectRight = false;
				this.rectDown = true;
			} else {
				this.rectRight = true;
			}
		}

		else if (this.rectDown == true) {
			this.y++;
			if (this.y == 500) {
				this.rectDown = false;
				this.rectLeft = true;
			} else {
				this.rectDown = true;
			}
		}

		else if (this.rectLeft == true) {
			this.x--;
			if (this.x == 50) {
				this.rectLeft = false;
				this.rectUp = true;
			} else {
				this.rectLeft = true;
			}
		}

		else if (this.rectUp == true) {
			this.y--;
			if (this.y == 50) {
				this.rectUp = false;
				this.rectRight = true;
			} else {
				this.rectUp = true;
			}
		}

	}

	public void render(Graphics graphics) {
		graphics.drawRect((float) this.x, (float) this.y, 80, 40);
	}

}
