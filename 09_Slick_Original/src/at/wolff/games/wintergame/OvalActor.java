package at.wolff.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class OvalActor implements Actor{
	private double x, y;
	private boolean circleRight = true;
	private boolean circleLeft = false;

	public OvalActor(double x, double y) {
		super();
		this.x = x;
		this.y = y;

	}

	public void update(GameContainer gc, int delta) {
		
		if (this.circleRight == true) {
			this.x++;
			if (this.x == 700) {
				this.circleRight = false;
				this.circleLeft = true;
			} else {
				this.circleRight = true;
			}

		} else if (this.circleLeft == true) {
			this.x--;
			if (this.x == 50) {
				this.circleLeft = false;
				this.circleRight = true;
			} else {
				this.circleLeft = true;
			}
		}
	}

	public void render(Graphics graphics) {
		graphics.drawOval((float) this.x, (float) this.y, 40, 80);
	}

}
