package at.wolff.games.wintergame;

import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class ShootingStar implements Actor {
	private float x, y;
	private float counter;
	public float randomNumber;
	private int timeSinceStartingOfCount;
	private boolean isVisible;

	public ShootingStar(float x, float y) {
		super();
		this.x = x;
		this.y = y;
		this.timeSinceStartingOfCount = 0;
		this.isVisible = false;
	}

	private void setRandomNumber() {
		this.randomNumber = getRandomNumber(15) + 5;
	}

	private int getRandomNumber(int range) {
		Random r = new Random();
		return r.nextInt(range);
	}
	

	public void update(GameContainer gc, int delta) {
		System.out.println("delta: " + delta + " time:" + timeSinceStartingOfCount);
		System.out.println(isVisible);
		System.out.println(randomNumber);


		
		this.timeSinceStartingOfCount += delta;

		if (this.timeSinceStartingOfCount > (this.randomNumber * 1000) && this.timeSinceStartingOfCount < (this.randomNumber * 1000 + 5000)) {

			this.isVisible = true;
			this.x = (float) (this.x + delta * 0.2);
			this.y = (float) (0.0005 * this.x * this.x + -0.001 * this.x + 300);
			

		} else if (timeSinceStartingOfCount > (this.randomNumber * 1000 + 4999)) {

			this.isVisible = false;
			this.timeSinceStartingOfCount = 0;
			this.x = -20;
			this.y = 300;
			setRandomNumber();
			
		}
	}

	public void render(Graphics graphics) {
		graphics.setColor(Color.yellow);
		if (this.isVisible == true) {
			graphics.fillOval(this.x, this.y, 20, 20, 4);
		}
		graphics.setColor(Color.white);

	}

}